import { StyleSheet, Platform } from "react-native";
import { Dimensions } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { colors } from "../../styles/color";
import {
  HeadingMediumBold,
  HeadingXxSmallBold,
  HeadingXxSmallRegular,
  ButtonSmallBold,
  BodyTextXLarge,
  HeadingXxxSmallBold,
} from "../../styles/typography";
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const isPortrait = height > width;
const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },

  bodyContainer: {
    alignItems: "center",
    backgroundColor: colors.bodyBackgroundColor,
    paddingHorizontal: wp("3.35%"),
    paddingVertical: hp("3.65%"),
    gap: 24,
    flex: 1,
    justifyContent: "center",
    ...Platform.select({
      web: {
        paddingHorizontal: wp("3.35%"),
        paddingVertical: hp("3.65%"),
      },
      ios: {
        paddingVertical: height < 745 ? hp ("2%") : hp("3.65%")
      },
    }),
  },
  innerBodyContainer: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: colors.gray,
    borderRadius: 16,
    width: wp("63.3%"),
    height: hp("51%"),
    overflow: "hidden",
    borderBottomWidth: 1,
  },
  innerBodyContainerNot: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: colors.gray,
    borderRadius: 16,
    width: wp("63.3%"),
    minHeight: hp("57.6%"),
    overflow: "hidden",
    borderBottomWidth: 1,
  },
  innerMainTitle: {
    width: wp("93.4%"),
    height: hp("5.45%"),
    alignItems: "center",
    justifyContent: "center",
  },
  innerMainTitleText: {
    fontSize: HeadingMediumBold.size,
    lineHeight: HeadingMediumBold.lineHeight,
    color: colors.textColor,
  },
  bodyTitle: {
    backgroundColor: colors.neutralGrayColor,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: wp("63.3%"),
    height: hp("7.7%"),

    ...Platform.select({
      ios: {
        height: isPortrait ? hp("6%") : hp("7.7%"),
      },
    }),
  },
  bodyTitleText: {
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
    height: 30,
    paddingHorizontal: 24,
    alignItems: "center",
    color: colors.textColor,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.secondary,
    gap: 16,
    paddingVertical: hp("1.35%"),
    paddingHorizontal: wp("2%"),
    width: wp("63.3%"),
    height: hp("6.6%"),
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  rowAddress: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.secondary,
    paddingHorizontal: wp("2%"),
    paddingVertical: hp("1.35%"),
    width: wp("63.3%"),
    height: hp("11.05%"),
    gap: 16,
    borderBottomWidth: 0,
    borderBottomColor: colors.gray,
  },
  firstContent: {
    width: wp("14.4%"),
    gap: 8,
    alignContent: "center",
    color: colors.textColor,
  },
  firstContentAddress: {
    width: wp("14.4%"),
    gap: 8,
    alignContent: "center",
    color: colors.textColor,
  },

  secondContent: {
    width: wp("43.55%"),
    textAlign: "center",
    color: colors.textColor,
  },
  secondContentAddress: {
    width: wp("43.55%"),
    color: colors.textColor,
  },
  innerBodyBoldText: {
    alignItems: "center",
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
    fontWeight: "600",
    color: colors.textColor,
  },
  innerBodyText: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: HeadingXxSmallRegular.size,
    lineHeight: height<1024 ? 30 : 37,
    color: colors.textColor,
  },
  iconStyle: {
    alignItems: "center",
    lineHeight: ButtonSmallBold.lineHeight,
    paddingTop: 3,
    paddingLeft: 3,
    width: 27,
    height: 27,
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    gap: 8,
    paddingHorizontal: wp("1.66%"),
    paddingVertical: hp("1.15%"),
    justifyContent: "center",
  },
  btnModify: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderRadius: 4,
    width: 184,
    height: 44,
    alignContent: "center",
    borderColor: colors.primary,
    fontSize: ButtonSmallBold.size,
    lineHeight: ButtonSmallBold.lineHeight,
  },
  outerContainer: {
    width: wp("91.2%"),
    gap: 66,
    flexDirection: "row",
    justifyContent: "center",
    flex: 1,
  },
  sideScrollDiv: {
    width: wp("22.35%"),
    height: hp("51%"),
    ...Platform.select({
      web: {
        height: width < 1490 ? hp ("57%") : width< 1900 ? hp("54%"): hp("53.3%")
      },
      ios: {
        height: height < 745 ? hp ("51%") : width==1194 ? hp("50.5%"): hp("50%")
      },
    }),
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
  },
  btnSideDiv: {
    display: "flex",
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: wp("0.65%"),
    paddingVertical: hp("0.9%"),
    gap: 4,
    alignItems: "center",
    width: wp("19.7%"),
    height: hp("5.65%"),
    borderColor: colors.borderColor,
    backgroundColor: colors.secondary,
  },
  btnSideDivActive: {
    display: "flex",
    flexDirection: "row",
    borderWidth: 2,
    borderRadius: 4,
    paddingHorizontal: wp("0.65%"),
    paddingVertical: hp("0.9%"),
    gap: 4,
    alignItems: "center",
    width: wp("19.7%"),
    height: hp("5.65%"),
    borderColor: colors.primary,
    backgroundColor: colors.neuralBlueLightColor,
  },
  sideTitleText: {
    height: hp("6.15%"),
    width: wp("22.35%"),
    backgroundColor: colors.neutralGrayColor,
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
    color: colors.textColor,
    paddingHorizontal: wp("1.35%"),
    paddingVertical: hp("1.35%"),
    gap: 4,
  },
  count: {
    fontSize: HeadingXxxSmallBold.size,
    lineHeight: HeadingXxxSmallBold.lineHeight,
    color: colors.greyTextColor,
    width: wp("1.5%"),
    height: hp("3.05%"),
    display: "flex",
  },
  sideText: {
    color: colors.textColor,
    fontSize: BodyTextXLarge.size,
    lineHeight: 34,
    width: wp("16.5%"),
    height: hp("3.85%"),
  },
  scrollableGp: {
    width: wp("22.35%"),
    gap: wp("1%"),
    paddingHorizontal: wp("1.35%"),
    paddingVertical: hp("1.8%"),
    ...Platform.select({
      web: {
        paddingHorizontal: wp("1%"),
      },
    }),
    overflow: "hidden",
  },
  scrollableGroup: {
    width: wp("22.35%"),
    overflow: "hidden",
  },
});

export default styles;
