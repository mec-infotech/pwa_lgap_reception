import React from "react";
import Dialog from "../../components/basics/Dialog";
import { Int32 } from "react-native/Libraries/Types/CodegenTypes";

type LogoutProps = {
  dialogHeight?: Int32;
  iconVisible?: boolean;
  dialogTitle?: string;
  text?: string;
  firstButtonText?: string;
  secondButtonText?: string;
  secondButtonVisible?: boolean;
  onFirstButtonPress?: ()  => void;
  onSecondButtonPress?: ()  => void;
};

export const Logout = (props: LogoutProps) => {
  const defaultOnPress = () => {
    console.log("Default onPress action");
  };

  return (
    <Dialog
      dialogTitle="ログアウト"
      text="ログアウトしますか？"
      onFirstButtonPress={props.onFirstButtonPress || defaultOnPress}
      firstButtonText="ログアウト"
      iconVisible={false}
      secondButtonVisible={true}
      secondButtonText="キャンセル"
      onSecondButtonPress={props.onSecondButtonPress || defaultOnPress}
      containerHeight={332}
      containerGap={40}
      btnContainerHeight={120}
    />
  );
};
