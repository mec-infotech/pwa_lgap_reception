import React, { useState } from "react";
import {
  StatusBar,
  SafeAreaView,
  TextInput,
  Pressable,
  Modal,
  FlatList,
} from "react-native";
import styles from "./EventListStyle";
import { Header } from "../../components/basics/header";
import { View } from "../../components/Themed";
import { Button } from "../../components/basics/Button";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import { TagLink } from "../../components/basics/TagLink";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  Feather,
} from "@expo/vector-icons";
 
export const EventList = () => {
  const [showInputs, setShowInputs] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState("最終更新日が新しい");
  const defaultOnPress = () => {};
  const handleCalendarPress = () => {};
  const toggleInputs = () => {
    setShowInputs(!showInputs);
  };
 
  const handleDropdownPress = () => {
    setIsDropdownVisible(!isDropdownVisible);
  };
 
  const dropdownData = [
    { label: "最終更新日が新しい", value: "newest" },
    { label: "最終更新日が古い", value: "oldest" },
  ];
 
  const handleDropdownSelect = (value: any) => {
    setSelectedOption(value);
    setIsDropdownVisible(false);
  };
 
  const DATA = [
    {
      id: 1,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 2,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場、C会場、D会場",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 3,
      eventName: "出茂マラソン大会2024",
      venue: "会場1",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 4,
      eventName:
        "出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会",
      venue: "会場1",
      eventPeriod: "",
    },
    {
      id: 5,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 6,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "",
    },
    {
      id: 7,
      eventName: "出茂マラソン大会2023",
      venue: "会場2",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 8,
      eventName: "出茂マラソン大会",
      venue: "C会場",
      eventPeriod: "",
    },
  ];
 
  const ITEMS_PER_PAGE = 6;
  const [page, setPage] = useState(1);
 
  const renderHeader = () => (
    <View style={styles.header}>
      <HiraginoKakuText style={[styles.headerText, styles.eventHeaderText]}>
        イベント名
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.headerText, styles.venueHeaderText]}>
        会場
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.headerText, styles.eventDateText]}>
        イベント期間
      </HiraginoKakuText>
    </View>
  );
 
  const renderTableItem = ({ item }: { item: any }) => (
    <View style={styles.row}>
      <TagLink
        url="#"
        text={item.eventName}
        pressableStyle={styles.eventBodyPressableText}
        textStyle={[styles.cell, styles.eventBodyText]}
        numberOfLines={1}
        normal
      />
      <HiraginoKakuText
        style={[styles.cell, styles.venueBodyText]}
        numberOfLines={1}
        normal
      >
        {item.venue}
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.cell, styles.eventDateBodyText]} normal>
        {item.eventPeriod}
      </HiraginoKakuText>
    </View>
  );
 
  const getPageData = () => {
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return DATA.slice(startIndex, endIndex);
  };
 
  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="受付するイベントを選んでください"
        buttonName="ログアウト"
      />
      <FlatList
        data={[{ key: "content" }]}
        renderItem={() => (
          <View style={styles.bodyContainer}>
            <View
              style={[styles.firstChildContainer, { gap: showInputs ? 16 : 8 }]}
            >
              <View style={styles.infoContainer}>
                <View style={styles.parentInputContainer}>
                  <View style={styles.childInputContainer}>
                    <View style={styles.labelContainer}>
                      <HiraginoKakuText style={styles.labelText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.input}
                        placeholder="イベントタイトル"
                        placeholderTextColor={colors.placeholderTextColor}
                      />
                    </View>
                  </View>
                </View>
                <>
                  <Pressable
                    style={styles.dropdownContainer}
                    onPress={toggleInputs}
                  >
                    <HiraginoKakuText style={styles.dropdownText}>
                      詳細検索
                    </HiraginoKakuText>
                    <Entypo
                      name={showInputs ? "chevron-up" : "chevron-down"}
                      size={24}
                      color={colors.primary}
                      style={styles.iconStyle}
                    />
                  </Pressable>
                  {showInputs && (
                    <View style={styles.hiddenContainer}>
                      <View style={styles.parentInputContainer}>
                        <View style={styles.childInputContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              会場
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.inputContainer}>
                            <TextInput
                              style={styles.input}
                              placeholder="会場名"
                              placeholderTextColor={colors.placeholderTextColor}
                            />
                          </View>
                        </View>
                      </View>
                      <View style={[styles.mainDateContainer]}>
                        <View style={styles.parentDateContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              開始日
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.childDateContainer}>
                            <View style={styles.dateInputContainer}>
                              <TextInput
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                              />
                              <Pressable
                                style={styles.calendarIconContainer}
                                onPress={handleCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                            </View>
                            <HiraginoKakuText style={styles.tildeText}>
                              〜
                            </HiraginoKakuText>
                            <View style={styles.dateInputContainer}>
                              <TextInput
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                              />
                              <Pressable
                                style={styles.calendarIconContainer}
                                onPress={handleCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                            </View>
                          </View>
                        </View>
                        <View style={styles.parentDateContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              終了日
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.childDateContainer}>
                            <View style={styles.dateInputContainer}>
                              <TextInput
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                              />
                              <Pressable
                                style={styles.calendarIconContainer}
                                onPress={handleCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                            </View>
                            <HiraginoKakuText style={styles.tildeText}>
                              〜
                            </HiraginoKakuText>
                            <View style={styles.dateInputContainer}>
                              <TextInput
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                              />
                              <Pressable
                                style={styles.calendarIconContainer}
                                onPress={handleCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  )}
                </>
              </View>
              <View style={styles.buttonContainer}>
                <Button
                  text="クリア"
                  onPress={defaultOnPress}
                  style={styles.grayMButton}
                  type="ButtonMediumGray"
                />
                <Button
                  text="検索"
                  onPress={defaultOnPress}
                  style={styles.PrimaryMButton}
                  type="ButtonMPrimary"
                />
              </View>
            </View>
            <View style={styles.mainPaginationContainer}>
              <View style={styles.parentPaginationContainer}>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      1-50 / 9,999 件中
                    </HiraginoKakuText>
                  </View>
                  <Pressable
                    style={styles.sortingContainer}
                    onPress={handleDropdownPress}
                  >
                    <MaterialCommunityIcons
                      name="swap-vertical"
                      size={20}
                      color={colors.activeCarouselColor}
                    />
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {selectedOption}
                    </HiraginoKakuText>
                    <Entypo
                      name="chevron-down"
                      size={20}
                      color={colors.greyTextColor}
                      style={styles.dropdownIconStyle}
                    />
                  </Pressable>
 
                  {/* Dropdown Modal */}
                  <Modal
                    visible={isDropdownVisible}
                    transparent={true}
                    animationType="fade"
                    onRequestClose={() => setIsDropdownVisible(false)}
                  >
                    <View style={styles.modalContainer}>
                      <FlatList
                        data={dropdownData}
                        keyExtractor={(item) => item.value}
                        renderItem={({ item }) => (
                          <Pressable
                            style={styles.dropdownItem}
                            onPress={() => handleDropdownSelect(item.label)}
                          >
                            <HiraginoKakuText
                              style={styles.paginationCount}
                              normal
                            >
                              {item.label}
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                      />
                    </View>
                  </Modal>
                </View>
                <View style={styles.tableContainer}>
                  <FlatList
                    ListHeaderComponent={renderHeader}
                    data={getPageData()}
                    renderItem={renderTableItem}
                    keyExtractor={(item) => item.id.toString()}
                  />
                </View>
              </View>
              <View style={styles.onChangePageContainer}>
                <View style={styles.previousButtonsContainer}>
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.skipBackButton}
                    type="ButtonSDisable"
                    icon={
                      <Feather
                        name="skip-back"
                        size={20}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="center"
                  />
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.chevronLeftButton}
                    type="ButtonSDisable"
                    icon={
                      <Feather
                        name="chevron-left"
                        size={20}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="center"
                  />
                </View>
                <View style={styles.pageNumberContainer}>
                  <Button
                    text="1"
                    onPress={defaultOnPress}
                    style={styles.numOneButton}
                    type="ButtonSPrimary"
                  />
                  <Button
                    text="2"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                  <Button
                    text="3"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                  <HiraginoKakuText style={styles.threeDots} normal>
                    …
                  </HiraginoKakuText>
                  <Button
                    text="50"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                </View>
                <View style={styles.nextButtonsContainer}>
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.rightButtons}
                    type="ButtonSGray"
                    icon={
                      <Feather
                        name="chevron-right"
                        size={20}
                        color={colors.textColor}
                      />
                    }
                    iconPosition="center"
                  />
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.rightButtons}
                    type="ButtonSGray"
                    icon={
                      <Feather
                        name="skip-forward"
                        size={20}
                        color={colors.textColor}
                      />
                    }
                    iconPosition="center"
                  />
                </View>
              </View>
            </View>
          </View>
        )}
      />
    </SafeAreaView>
  );
};
 