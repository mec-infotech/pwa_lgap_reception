import React, { useState } from "react";
import { StatusBar, SafeAreaView, Pressable, TextInput } from "react-native";
import styles from "./CheckInEditStyle";
import { Header } from "../../components/basics/header";
import { View } from "../../components/Themed";
import { Footer } from "../../components/basics/footer";
import { HiraginoKakuText } from "../../components/StyledText";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import { Button } from "../../components/basics/Button";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export const CheckInEdit = () => {
  const [selectedOption, setSelectedOption] = useState("");

  const handleSelectOption = (option: string) => {
    setSelectedOption((prevOption) => (prevOption === option ? "" : option));
  };

  const defaultOnPress = () => {};

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header titleName="受付内容修正" buttonName="受付をやめる" />
        <View style={styles.bodyContainer}>
          <HiraginoKakuText style={styles.titleText}>
            受付内容を修正してください
          </HiraginoKakuText>
          <View style={styles.container}>
            <View style={styles.nameLabelContainer}>
              <HiraginoKakuText style={styles.labelNameText}>
                お名前
              </HiraginoKakuText>
              <View style={styles.nameInputsContainer}>
                <TextInput
                  style={[styles.FirstNameInput, styles.FirstNameInputText]}
                />
                <TextInput
                  style={[styles.LastNameInput, styles.LastNameInputText]}
                />
              </View>
            </View>
            <View style={styles.nameLabelContainer}>
              <HiraginoKakuText style={styles.labelNameText}>
                お名前（カナ）
              </HiraginoKakuText>
              <View style={styles.nameInputsContainer}>
                <TextInput
                  style={[styles.FirstNameInput, styles.FirstNameInputText]}
                />
                <TextInput
                  style={[styles.LastNameInput, styles.LastNameInputText]}
                />
              </View>
            </View>
            <View style={styles.birthDateContainer}>
              <HiraginoKakuText style={styles.labelBirthDateText}>
                生年月日
              </HiraginoKakuText>
              <View style={styles.birthDateInputsContainer}>
                <View style={styles.birthDateInput}>
                  <HiraginoKakuText
                    style={styles.birthDateInputText}
                    normal
                  ></HiraginoKakuText>
                  <Pressable
                    style={styles.calendarIconContainer}
                    onPress={defaultOnPress}
                  >
                    <MaterialIcons
                      name="calendar-today"
                      size={22}
                      color={colors.activeCarouselColor}
                      style={styles.calendarIcon}
                    />
                  </Pressable>
                </View>
              </View>
            </View>
            <View style={styles.genderContainer}>
              <HiraginoKakuText style={styles.labelGenderText}>
                性別
              </HiraginoKakuText>
              <View style={styles.genderRadioContainer}>
                <View style={styles.radioContainer}>
                  <RadioPanel
                    selected={selectedOption === "M"}
                    onPress={() => handleSelectOption("M")}
                    radioBtnText="男性"
                  />
                </View>
                <View style={styles.radioContainer}>
                  <RadioPanel
                    selected={selectedOption === "F"}
                    onPress={() => handleSelectOption("F")}
                    radioBtnText="女性"
                  />
                </View>
                <View
                  style={[styles.radioContainer, styles.radioKaitouContainer]}
                >
                  <RadioPanel
                    selected={selectedOption === "N"}
                    onPress={() => handleSelectOption("N")}
                    radioBtnText="回答しない"
                  />
                </View>
              </View>
            </View>
            <View style={styles.postCodeContainer}>
              <HiraginoKakuText style={styles.labelPostCodeText}>
                郵便番号
              </HiraginoKakuText>
              <View style={styles.postCodeInputsContainer}>
                <TextInput
                  style={[styles.postCodeInput, styles.postCodeInputText]}
                />
                <Button
                  text="住所検索"
                  onPress={defaultOnPress}
                  style={styles.searchButton}
                  type="ButtonMSecondary"
                />
              </View>
            </View>
            <View style={styles.addressContainer}>
              <HiraginoKakuText style={styles.labelAddressText}>
                住所
              </HiraginoKakuText>
              <TextInput
                style={[styles.addressInput, styles.addressInputText]}
              />
            </View>
          </View>
        </View>
        <Footer rightButtonText="確認する" hasPreviousButton={false}></Footer>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};

const RadioButton = (props: any) => {
  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: colors.fullyTransparentBlack,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: colors.primary,
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <Pressable onPress={onPress} style={styles.radioPressable}>
      <View style={styles.radioButtonIcon}>
        <RadioButton
          selected={selected}
          style={[styles.radioButton, selected && styles.selectedRadioButton]}
        />
      </View>
      <View style={styles.radioTextContainer}>
        <HiraginoKakuText style={styles.radioText} normal>
          {radioBtnText}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );
};
