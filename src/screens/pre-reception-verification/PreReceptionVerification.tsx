import React from "react";
import Dialog from "../../components/basics/Dialog";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import styles from "./PreReceptionVerificationStyles";

export const PreReceptionVerification = () => {
  return (
    <Dialog
      dialogTitle="受付を開始しますか？"
      text="ログアウトしますか？"
      firstButtonText="受付開始"
      iconVisible={false}
      secondButtonVisible={true}
      secondButtonText="キャンセル"
      containerHeight={493}
      containerGap={32}
      dialogBodyGap={40}
      btnContainerHeight={120}
    >
      <View style={styles.ListContainter}>
        <HiraginoKakuText style={styles.subTitleText}>
          マラソン大会
        </HiraginoKakuText>
        <View style={styles.upperContainer}>
          <HiraginoKakuText style={styles.innerLabel}>会場</HiraginoKakuText>
          <HiraginoKakuText style={styles.innerText} normal>
            A会場
          </HiraginoKakuText>
        </View>
        <View style={styles.LowerContainer}>
          <HiraginoKakuText style={styles.innerLabel}>
            イベント期間
          </HiraginoKakuText>
          <HiraginoKakuText style={styles.innerText} normal>
            2023/01/02〜2023/01/03
          </HiraginoKakuText>
        </View>
      </View>
    </Dialog>
  );
};
