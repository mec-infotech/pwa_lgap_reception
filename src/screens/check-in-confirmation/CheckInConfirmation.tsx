import React from "react";
import {SafeAreaView, View, Dimensions} from "react-native";
import styles from "./CheckInConfirmationStyles";
import {StatusBar} from "react-native";
import {Header} from "../../components/basics/header";
import {HiraginoKakuText} from "../../components/StyledText";
import {Footer} from "../../components/basics/footer";
import {Button} from "../../components/basics/Button";
import {MaterialIcons} from "@expo/vector-icons";
import {colors} from "../../styles/color";

export const CheckInConfirmation = () => {
  const screenWidth = Dimensions.get("window").width;
  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content"></StatusBar>
      <Header titleName="受付内容確認" buttonName="受付をやめる"></Header>
      <View style={styles.bodyContainer}>
        <View style={styles.innerMainTitle}>
          <HiraginoKakuText style={styles.innerMainTitleText}>
            この内容で受付しますか？
          </HiraginoKakuText>
        </View>

        <View style={styles.innerBodyContainer}>
          <View style={styles.bodyTitle}>
            <HiraginoKakuText style={styles.bodyTitleText}>
              受付内容
            </HiraginoKakuText>
            <View style={styles.buttonContainer}>
              <Button
                text="内容を修正する"
                type="ButtonMSecondary"
                style={styles.btnModify}
                icon={
                  <MaterialIcons
                    name="mode-edit"
                    size={24}
                    color={colors.primary}
                    style={styles.iconStyle}
                  />
                }
                iconPosition="behind"
              ></Button>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                お名前
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContent}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                出茂 太郎
              </HiraginoKakuText>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                お名前（カナ）
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContent}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                イズモ タロウ
              </HiraginoKakuText>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                生年月日
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContent}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                2000年01月02日
              </HiraginoKakuText>
            </View>
          </View>
          {/* // 性別(Female/male/other) is optional. */}
          <View style={styles.row}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                性別
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContent}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                男性
              </HiraginoKakuText>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                郵便番号
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContent}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                515-0004
              </HiraginoKakuText>
            </View>
          </View>

          <View style={styles.rowAddress}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                住所
              </HiraginoKakuText>
            </View>
            <View style={styles.secondContentAddress}>
              <HiraginoKakuText style={styles.innerBodyText} normal>
                三重県松阪市なんとか町11-2
                マンション名102あああああああああああああああああああああああああああああああああああああああ
              </HiraginoKakuText>
            </View>
          </View>
        </View>
      </View>
      <Footer
        rightButtonText="受付する"
        hasPreviousButton={false}
        showNextIcon={false}
      ></Footer>
    </SafeAreaView>
  );
};

export default CheckInConfirmation;
