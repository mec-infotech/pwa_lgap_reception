import React, { useState } from "react";
import {
  View,
  StatusBar,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import styles from "./SelectReceptionMethodStyles";
import { Header } from "../../components/basics/header";
import { AntDesign } from "@expo/vector-icons";
import { Certification } from "../certification/Certification";

export const SelectReceptionMethod = () => {
  const [isMenuVisible, setMenuVisible] = useState(false);
  const [isCertificationModalVisible, setCertificationModalVisible] =
    useState(false);

  const showMenu = () => {
    setMenuVisible(!isMenuVisible);
  };

  const closeMenu = () => {
    if (isMenuVisible && !isCertificationModalVisible) {
      setMenuVisible(false);
    }
  };

  const showCertification = () => {
    setMenuVisible(false);
    setCertificationModalVisible(!isCertificationModalVisible);
  };

  const handleCancelButton = () => {
    setCertificationModalVisible(false);
  };

  return (
    <TouchableWithoutFeedback onPress={closeMenu}>
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header
          titleName="受付"
          buttonName=""
          buttonWidth={52}
          onPress={showMenu}
          icon={
            <AntDesign
              name="ellipsis1"
              size={28}
              style={styles.dotIcon}
              color="black"
            />
          }
          iconPosition="center"
        ></Header>
        <View style={styles.bodyContainer}>
          <View style={styles.bodyTextContainer}>
            <HiraginoKakuText style={styles.bodyText}>
              [イベント名]の
            </HiraginoKakuText>
            <HiraginoKakuText style={styles.bodyText}>
              受付方法を選択してください
            </HiraginoKakuText>
          </View>
          <View style={styles.selectionContainer}>
            <View style={styles.qrContainer}>
              <View style={styles.qrRemark}>
                <HiraginoKakuText style={styles.qrRemarkText}>
                  ご利用には、[自治体アプリ]が必要です
                </HiraginoKakuText>
              </View>
              <View style={styles.qrCode}>
                <Image
                  source={require("../../assets/images/qr_code.png")}
                  style={styles.qrImage}
                ></Image>
              </View>
              <HiraginoKakuText style={styles.qrText}>
                自己QRで受付
              </HiraginoKakuText>
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.qrCode}>
                <Image
                  source={require("../../assets/images/input.png")}
                  style={styles.inputImage}
                ></Image>
              </View>
              <HiraginoKakuText style={styles.inputText}>
                この場で入力して受付
              </HiraginoKakuText>
            </View>
          </View>
        </View>
        {isMenuVisible && (
          <TouchableOpacity style={styles.menu} onPress={showCertification}>
            <HiraginoKakuText style={styles.menuLabel} normal>
              {" "}
              管理者画面へ
            </HiraginoKakuText>
          </TouchableOpacity>
        )}
        {isCertificationModalVisible && (
          <Certification
            onFirstButtonPress={handleCancelButton}
            toggleModal={showCertification}
          />
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};
